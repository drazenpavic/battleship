<div class="col-xs-12 col-md-4 col-md-push-4 main-box">
	<form class="form-signin" role="form" method="get">
		<h2 class="form-signin-heading">Please login</h2>
		
		<input type="hidden" name="page" value="login" />

		<div class="form-group">
			<label for="email">Email address</label> <input type="email"
				id="email" name="email" class="form-control" required autofocus />
		</div>

		<div class="form-group">
			<label for="password">Password</label> <input
				type="password" name="password" id="password" class="form-control" required>
		</div>

		<div class="form-group">
			<input class="btn btn-lg btn-primary btn-block" name="action" type="submit" value="Login" />
		</div>
	</form>
</div>