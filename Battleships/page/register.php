<div class="col-xs-12 col-md-4 col-md-push-4 main-box">
	<form class="form-signin" role="form">
		<h2 class="form-signin-heading">Register</h2>

		<div class="form-group">
			<label for="email">Email address</label> <input type="email"
				id="email" name="email" class="form-control" required autofocus />
		</div>

		<div class="form-group">
			<label for="password">Password</label> <input
				type="password" name="password" id="password" class="form-control" required>
		</div>

		<div class="form-group">
			<label for="repeat-password">Repeat password</label> <input
				type="password" name="repeat-password" id="repeat-password" class="form-control" required>
		</div>

		<div class="form-group">
			<input class="btn btn-lg btn-primary btn-block" type="submit" value="Register"></input>
		</div>
	</form>
</div>