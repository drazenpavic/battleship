<div class="container">
	<div class="col-md-4 col-md-push-4 main-box">
		<h1>Start New Game</h1>

		<table class="map" cellspacing="10">
		  <?php 
		  
		  for ($i=0; $i<$size; $i++)
		  {
		  	echo '<tr>';
		  	
		  	for($j=0; $j<$size; $j++)
		  	{
		  		$position = ($i * $size) + $j;
		  		echo "<td data-position='$position'></td>";
		  	}
		  	
		  	echo '</tr>';
		  }
		  
		  ?>
		</table>
		
		<input type="button" class="start-new-game btn btn-primary" value="Publish game" />
		and wait for opponent
	</div>
</div>

<script>

	jQuery(document).ready(function($) {
		$('table.map td').click(function() {

			var count = $('table.map td.position').size();

			if($(this).hasClass('position') || count < 3) {
				$(this).toggleClass('position');
			}
		});

		$('.start-new-game').click(function() {
			var positions = [];
			
			var cells = $('table.map td.position');
			var size = cells.size();

			for(i=0; i<size; i++)
			{
				positions.push($(cells[i]).data('position'));
			}

			$.get('create-game.php', {
				positions: positions
			}, function() {
				window.location = 'index.php?page=my-games';
			});
		});
	});
  
</script>

