
<div class="col-md-12 main-box">
	<h1>Playing with ...</h1>
	
	<div class="col-md-6">
	
		<table class="map" cellspacing="10">
			  <?php 
			  
			  $fake = array();
			  
			  $fake[] = rand(0, 15);
			  $fake[] = rand(0, 15);
			  $fake[] = rand(0, 15);
			  
			  $bombs = array();
			  
			  $bombs = rand(0, 15);
			  $bombs = rand(0, 15);
			  $bombs = rand(0, 15);
			  $bombs = rand(0, 15);
			  $bombs = rand(0, 15);
			  
			  for ($i=0; $i<$size; $i++)
			  {
			  	echo '<tr>';
			  	
			  	for($j=0; $j<$size; $j++)
			  	{
			  		$position = ($i * $size) + $j;
			  		
			  		if(in_array($position, $fake))
			  		{
			  			echo "<td data-position='$position' class='position'>";
			  		}
			  		else
			  		{
			  			echo "<td data-position='$position'>";
			  		}

			  	}
			  	
			  	echo '</tr>';
			  }
			  
			  ?>
		</table>
	</div>
	
	<div class="col-md-6">
		<table class="map moves" cellspacing="10">
		  <?php 
		  
		  for ($i=0; $i<$size; $i++)
		  {
		  	echo '<tr>';
		  	
		  	for($j=0; $j<$size; $j++)
		  	{
		  		$position = ($i * $size) + $j;
		  		echo "<td data-position='$position'></td>";
		  	}
		  	
		  	echo '</tr>';
		  }
		  
		  ?>
		</table>
	</div>
</div>

<script>
	  jQuery(document).ready(function($) {
		$('table.moves td').click(function() {
			
			var hit = Math.floor(Math.random() * 2) + 1;

			if(!($(this).hasClass('hit') || $(this).hasClass('miss')))
			{
				if(hit == 2)
				{
					$(this).addClass('hit');
					$(this).append($('<span class="glyphicon glyphicon-ok"></span>'))
				}
				else
				{
					$(this).addClass('miss');
					$(this).append($('<span class="glyphicon glyphicon-remove"></span>'))
				}
			}
			
			
		});
	  });
</script>
