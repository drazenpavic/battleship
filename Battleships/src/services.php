<?php

class GameInfo
{
	private $id;
	private $player1;
	private $player2;
	private $next;
	private $status;
	private $created;
	
	public function getCreated()
	{
		return $this->created;
	}
	
	public function setCreated($created)
	{
		$this->created = $created;
	}
	
	public function getNext()
	{
		return $this->next;
	}
	
	public function setNext($next)
	{
		$this->next = $next;
	}
	
	public function getStatus()
	{
		return $this->status;
	}
	
	public function setStatus($status)
	{
		$this->status = $status;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function getPlayer1()
	{
		return $this->player1;
	}
	
	public function setPlayer1($name)
	{
		$this->player1 = $name;
	}
	
	public function getPlayer2()
	{
		return $this->player2;
	}
	
	public function setPlayer2($name)
	{
		$this->player2 = $name;
	}
	
}

class Services
{
	private function getConnection()
	{
		$host = "whoships.db.4432980.hostedresource.com";
		$user = "whoships";
		$password = "LogIn123#";
		$database = "whoships";
		
		$db = new mysqli($host, $user, $password, $database);
		
		if($db->connect_errno > 0)
		{
			die('Unable to connect to database [' . $db->connect_error . ']');
		}
		
		return $db;
	}
	
	public function register($username, $password)
	{
		$query = "INSERT INTO player (id, username, password) VALUES(?,?,?)";
		
		$stmt = $this->getConnection()->prepare($query);
		$stmt->bind_param('sss', uniqid(), $username, $password);

		return true;
	}
	
	public function isValidPassword($username, $password)
	{
		
		$query = "SELECT id FROM player WHERE username = ? AND password = ?;";
	
		$con = $this->getConnection();
		
		$stmt = $con->prepare($query);
		$stmt->bind_param("ss", $username, $password);
		$stmt->execute();
		
		$stmt->bind_result($id);
		$found = $stmt->fetch();
		$con->close();
		
		return $found;
	}
	
	public function startGame($username, $positions)
	{
		$gameQuery = "INSERT INTO game (id, player1, next) VALUES(?,?,?);";
		$positionQuery = "INSERT INTO positions(id, player_id, game_id, position) VALUES(?,?,?,?);";
		
		$gameId = uniqid();
		$con = $this->getConnection();
		
		$gs = $con->prepare($gameQuery);
		$gs->bind_param("sss", $gameId, $username, $username);
		$gs->execute();
		$gs->close();
		
		foreach ($positions as $position)
		{
			$ps = $con->prepare($positionQuery);
			$ps->bind_param("ssss", uniqid(), $username, $gameId, $position);
			$ps->execute();
			$ps->close();
		}
		
		$con->close();
	
		return true;
	}
	
	public function joinGame($gameId, $username, $positions)
	{
		$gameQuery = "UPDATE game SET player2 = ? WHERE id = ?;";
		$positionQuery = "INSERT INTO positions(id, player_id, game_id, position) VALUES(?,?,?,?);";
		
		$con = $this->getConnection();
		
		$gs = $con->prepare($gameQuery);
		$gs->bind_param("ss", $username, $gameId);
		$gs->execute();
		$gs->close();
		
		foreach ($positions as $position)
		{
			$ps = $con->prepare($positionQuery);
			$ps->bind_param("ssss", uniqid(), $username, $gameId, $position);
			$ps->execute();
			$ps->close();
		}
		
		$con->close();
		
		return true;
	}
	
	public function listGames()
	{
		$query = "SELECT id, player1, player2, next, status, created 
				  FROM game WHERE player2 IS NULL
				  ORDER BY created DESC;";
		
		$con = $this->getConnection();
		$stmt = $con->prepare($query);
		$stmt->execute();
		$stmt->bind_result($id, $player1, $player2, $next, $status, $created);
		$games = array();
		while($stmt->fetch())
		{
			$game = new GameInfo();
			$game->setId($id);
			$game->setPlayer1($player1);
			$game->setPlayer2($player2);
			$game->setNext($next);
			$game->setStatus($status);
			$game->setCreated($created);
			
			$games[] = $game;
		}
		
		$con->close();
		
		return $games;
	}
	
	public function listUserGames($username)
	{
		$query = "SELECT * FROM game WHERE player1 = ? OR player2 = ?;";
		
		$con = $this->getConnection();
		$stmt = $con->prepare($query);
		$stmt->bind_param("ss", $username, $username);
		$stmt->execute();
		$stmt->bind_result($id, $player1, $player2, $next, $status, $created);
		$games = array();
		while($stmt->fetch())
		{
			$game = new GameInfo();
			$game->setId($id);
			$game->setPlayer1($player1);
			$game->setPlayer2($player2);
			$game->setNext($next);
			$game->setStatus($status);
			$game->setCreated($created);
				
			$games[] = $game;
		}
		
		$con->close();
		
		return $games;
	}
	
	public function makeMove($gameId, $username, $pozition)
	{
		$query = "INSERT INTO moves (id, game_id, player_id, move);";
		
		$con = $this->getConnection();
		$stmt = $con->prepare($query);
		$stmt->bind_param("sssi", uniqid(), $gameId, $username, $pozition);
		$stmt->execute();
		
		$con->close();
		
		return true;
	}
	
}
