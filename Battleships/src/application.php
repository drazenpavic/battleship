<?php

include_once 'src/services.php';

session_start();

class Application
{
	private static $app;
	
	public static function getInstance()
	{
		if(Application::$app == null)
		{
			Application::$app = new Application();
		}
		
		return Application::$app;
	}
	
	public function getServices()
	{
		return new Services();
	}
	
	public function getUsername()
	{
		return $_SESSION['username'];
	}
	
	public function createGame($positions)
	{
		$username = $this->getUsername();
		$this->getServices()->startGame($username, $positions);
	}
	
	public function register($username, $password)
	{
		return $this->getServices()->register($username, $password);
	}
	
	public function getMyGames()
	{
		return $this->getServices()->listUserGames($this->getUsername());
	}
	
	public function login($username, $password)
	{
		$status = $this->getServices()->isValidPassword($username, $password);
		
		if($status !== false)
		{
			$_SESSION['username'] = $username;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function joinGame($gameId, $positions)
	{
		$username = $this->getUsername();
		return $this->getServices()->joinGame($gameId, $username, $positions);
	}
	
	public function logout()
	{
		$_SESSION['username'] = false;
	}
	
	public function isInRole($role)
	{
		if('player' == $role)
		{
			return $_SESSION['username'] !== false;
		}
		
		return false;
	}
}
