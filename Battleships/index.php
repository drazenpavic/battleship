<?php
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	
	include_once 'src/application.php';
	
	$size = 4;
	
	$app = Application::getInstance();
	
	if(array_key_exists('action', $_REQUEST))
	{	
		$action = $_REQUEST['action'];
		
		if($action == 'Login')
		{
			$username = $_REQUEST['email'];
			$password = $_REQUEST['password'];
			
			$status = $app->login($username, $password);
			
			if($status === true)
			{
 				header("Location: index.php?page=new-game");
 				die();
			}
		}
		else if($action == 'logout')
		{
			$app->logout();
			header('Location: index.php?page=login');
		}
	}	

?><!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Battleships 6.0</title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="style.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-default navbar-static-top navbar-inverse" role="navigation">
		  <div class="container-fluid">

		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="index.php">Battleships</a>
		    </div>
		
		    <div class="collapse navbar-collapse" id="menu">
		      <ul class="nav navbar-nav navbar-right">
		        <li><a href="index.php?page=new-game">Start Game</a></li>
		        <li><a href="index.php?page=join">Join Game</a></li>
		        <li><a href="index.php?page=my-games">My Games</a></li>
		        
		        <?php 
		        	
		        	if($app->isInRole('player'))
		        	{
		        		echo '<li><a href="index.php?action=logout">Logout</a></li>';
		        	}
		        	else
		        	{
		        		echo '<li><a href="index.php?page=login">Login</a></li>';
		        	}
		        ?>
		      </ul>
		    </div>
		  </div>
		</nav>
		
		<div class="container">
		
			<?php 
				
				if($app->isInRole('player'))
				{
					if(array_key_exists('page', $_GET))
					{
						$page = $_GET['page'];
					}
					else
					{
						$page = 'new-game';	
					}
					
					include "page/$page.php";
					 
				}
				else
				{
					include "page/login.php";
				}
			?>
		</div>
	</body>
</html>